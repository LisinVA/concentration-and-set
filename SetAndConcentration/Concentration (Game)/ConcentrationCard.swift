//
//  ConcentrationCard.swift
//  Concentration (Game)
//
//  Created by zweqi on 19/02/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

// the Model of the Card

import Foundation

struct ConcentrationCard: Hashable {
    
    // MARK: - Public Properties
    
    var isFaceUp = false
    var isMatched = false
    
    // MARK: - Card Identification
    
    private var identifier: Int
    
    private static var identifierFactory = 0
    private static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    // MARK: - Initialization
    
    init() {
        self.identifier = ConcentrationCard.getUniqueIdentifier()
    }
    
    // MARK: - Hashable
    
    var hashValue: Int { return identifier }
    
    static func ==(lhs: ConcentrationCard, rhs: ConcentrationCard) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}

