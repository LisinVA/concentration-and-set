//
//  ConcentrationThemeChooserViewController.swift
//  Concentration (Game)
//
//  Created by zweqi on 19/02/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class ConcentrationThemeChooserViewController: UIViewController, UISplitViewControllerDelegate {
    
    // MARK: - Changing Theme
    
    private var splitViewConcentrationViewController: ConcentrationViewController? {
        let vc = splitViewController?.viewControllers.last
        return (vc?.contents as? ConcentrationViewController) ?? nil
    }
    
    @IBAction func changeTheme(_ sender: Any) {
        if let cvc = splitViewConcentrationViewController {
            // iPad/iPhone+
            // just change theme in opened view of Detail VC (without creating new VC)
            if let themeName = (sender as? UIButton)?.currentTitle, let theme = ConcentrationThemeChooserViewController.themes[themeName] {
                cvc.title = themeName
                cvc.theme = theme
            }
        } else if let cvc = lastSeguedToConcentrationViewController {
            // iPhone
            // change theme and push this VC onto main screen (without creating new VC)
            if let themeName = (sender as? UIButton)?.currentTitle, let theme = ConcentrationThemeChooserViewController.themes[themeName] {
                cvc.title = themeName
                cvc.theme = theme
            }
            navigationController?.pushViewController(cvc, animated: true)
        } else {
            // performing a segue will create a new VC, pushing old VC off the heap
            performSegue(withIdentifier: "Choose Theme", sender: sender)
        }
    }
    
    // MARK: - Navigation

    // a strong pointer to VC to able to return any time we want
    // (via pushViewController method, avoiding segues)
    private var lastSeguedToConcentrationViewController: ConcentrationViewController?
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Choose Theme" {
            if let themeName = (sender as? UIButton)?.currentTitle, let theme = ConcentrationThemeChooserViewController.themes[themeName] {
                if let cvc = (segue.destination as? UINavigationController)?.topViewController as? ConcentrationViewController {
                    
                    cvc.title = themeName
                    cvc.theme = theme
                    // now we able to return to it (via push) any time we want, without a segue
                    lastSeguedToConcentrationViewController = cvc
                }
            }
        }
    }
    
    // MARK: - Split View Controller
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // make self a delegate of the split VC to be able to use splitViewController() function later
        splitViewController?.delegate = self
    }
    
    // make that on iPhones, when we run app and the theme is nil, we see Master (a theme chooser VC)
    // true = "I'll do the collapsing"
    func splitViewController(
        _ splitViewController: UISplitViewController,
        collapseSecondary secondaryViewController: UIViewController,    // detail
        onto primaryViewController: UIViewController                    // master
        ) -> Bool {
        
        // we don't want to collapse when secondary VC (Concentration game) has a nil theme
        if let cvc = (secondaryViewController as? UINavigationController)?.visibleViewController as? ConcentrationViewController {
            if cvc.theme == nil {
                return true
            }
        }
        return false
    }

    // MARK: - View Controller Lifecycle
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        // setting up a title for the first appearing in split view mode
        if let cvc = splitViewConcentrationViewController, cvc.title == nil {
            cvc.title = "Default Theme"
            lastSeguedToConcentrationViewController = cvc
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(
            alongsideTransition: nil,
            completion: { finished in
                if let cvc = self.splitViewConcentrationViewController {
                    if cvc.navigationController == nil {
                        // embedding Detail VC in Navigation Controller, if needed
                        let nc = UINavigationController()
                        self.splitViewController?.viewControllers.removeLast()
                        self.splitViewController?.viewControllers.append(nc)
                        nc.pushViewController(cvc, animated: true)
                    }
                } else if self.lastSeguedToConcentrationViewController != nil {
                    // skip choose theme menu if we transitioned from Split VC
                    self.navigationController?.pushViewController(self.lastSeguedToConcentrationViewController!, animated: true)
                }
            }
        )
    }
}

// MARK: - Constants

extension ConcentrationThemeChooserViewController {
    
    static let themes = [
        "Sports": "⚽️🏀🏈⚾️🎾🏐🏉🎱⛷🎳⛳️",
        "Animals": "🐶🐔🦊🐼🦀🐪🐓🐋🐙🦄🐵",
        "Faces": "😀😂😎😟😰😴🙄🤔😘😷"
    ]
}
