//
//  ConcentrationCard.swift
//  Concentration (Game)
//
//  Created by zweqi on 19/02/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

// the Model of the Game

import Foundation

struct ConcentrationGame {
    
    private(set) var cards = [ConcentrationCard]()
    
    private var indexOfOneAndOnlyFaceUpCard: Int? {
        get {
            // go through all the cards and see if there is the only one that's face up
            return cards.indices.filter { cards[$0].isFaceUp }.oneAndOnly
        }
        set {
            // go through all the cards and turn them all face down, except the oneAndOnlyFaceUpCard
            for index in cards.indices {
                cards[index].isFaceUp = (index == newValue)
            }
        }
    }
    
    mutating func chooseCard(at index: Int) {
        assert(cards.indices.contains(index), "ConcentrationGame.chooseCard(at: \(index)): chosen index not in the cards")
        
        // ignore a card that has already been matched
        if !cards[index].isMatched {
            // 1 card is faced up
            if let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index {
                // check if cards match
                if cards[matchIndex] == cards[index] {
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                }
                cards[index].isFaceUp = true
                
            // either no cards or 2 cards are faced up
            } else {
                indexOfOneAndOnlyFaceUpCard = index
            }
        }
    }
    
    init(numberOfPairsOfCards: Int) {
        assert(numberOfPairsOfCards > 0, "ConcentrationGame.init(at: \(numberOfPairsOfCards)): you must have at least one pair of cards")
        
        // in each pair of cards, both of them will have the same card.identifier
        for _ in 1...numberOfPairsOfCards {
            let card = ConcentrationCard()
            cards += [card, card]
        }
    }
}
