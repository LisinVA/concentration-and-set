//
//  SetViewController.swift
//  Set (Game)
//
//  Created by zweqi on 29/01/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

class SetViewController: UIViewController {
    
    // MARK: - Game Model
    
    var game = Game(numberOfCards: Game.GameConstants.maxNumberOfCardsInDeck)
    
    // MARK: - Card Animation
    
    lazy var animator = UIDynamicAnimator(referenceView: cardStackView)
    lazy var cardBehavior = CardBehavior(in: animator)
    
    // MARK: - Storyboard
    
    @IBOutlet weak var cardStackView: CardStackView! {
        didSet {
            animator.delegate = self
            
            // shuffle cards on Rotation gesture
            let rotation = UIRotationGestureRecognizer(
                target: self,
                action: #selector(shuffleCards(_:))
            )
            cardStackView.addGestureRecognizer(rotation)
        }
    }
    
    /// Deck on the screen that represents remaining cards in the main deck.
    @IBOutlet weak var newCardsPileButton: UIButton! {
        didSet {
            newCardsPileButton!.layer.cornerRadius = 5.0
            newCardsPileButton!.layer.borderWidth = 2.0
            
            // Adding tap gesture to deal three more cards
            let tap = UITapGestureRecognizer(
                target: self,
                action: #selector(dealThreeCards)
            )
            newCardsPileButton.addGestureRecognizer(tap)
        }
    }
    
    /// Deck on the screen that represents discarded cards.
    @IBOutlet weak var discardCardsPileButton: UIButton! {
        didSet {
            discardCardsPileButton!.layer.cornerRadius = 5.0
            discardCardsPileButton!.layer.borderWidth = 2.0
            discardCardsPileButton!.layer.borderColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1)
            discardCardsPileButton!.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }
    
    @IBOutlet weak var newGameButton: UIButton! {
        didSet { newGameButton!.layer.cornerRadius = 5.0 }
    }
    
    /// Begins a game with new cards and zero score.
    @IBAction func startNewGame(_ sender: UIButton) {
        view.bringSubview(toFront: cardStackView)
        cardStackView.cardViews.forEach { $0.removeFromSuperview() }
        
        game = Game(numberOfCards: Game.GameConstants.maxNumberOfCardsInDeck)
        game.deal(numberOfCards: Game.GameConstants.numberOfCardsFirstDeal)
        updateViewFromModel(for: .dealing)
    }
    
    @IBOutlet weak var dealThreeCardsButton: UIButton! {
        didSet { dealThreeCardsButton.isEnabled = false }
    }
    
    @IBAction func dealThreeCardsButtonTouched(_ sender: UIButton) {
        dealThreeCards()
    }
    
    /// Deals three cards, if there are any in deck.
    @objc private func dealThreeCards() {
        guard !game.cardsOnTable.isEmpty else { return }
        guard !game.cardsInDeck.isEmpty else { return }
        guard game.selectedCards.count != Game.GameConstants.numberOfCardsInSet else { return }
        
        game.deal(numberOfCards: Game.GameConstants.numberOfCardsInSet)
        updateViewFromModel(for: .dealing)
    }
    
    @IBOutlet weak var hintButton: UIButton! {
        didSet { hintButton.isEnabled = false }
    }
    
    /// Highlights available "Set" on table.
    @IBAction func showHint(_ sender: UIButton) {
        if cardStackView.cardViews.filter({ $0.state == .hinted }).isEmpty {
            game.addHint()
            updateViewFromModel(for: .highlighting)
            game.removeHint()
        }
    }
    
    @IBOutlet var gameScoreLabel: UILabel! {
        didSet { updateGameScoreLabel() }
    }
    
    /// Updates a text on label, according to current game score.
    private func updateGameScoreLabel() {
        let title = "Score:  \(game.score)"
        let attributes: [NSAttributedStringKey: Any] = [
            .foregroundColor: #colorLiteral(red: 0.6679978967, green: 0.4751212597, blue: 0.2586010993, alpha: 1).withAlphaComponent(CGFloat(0.50)),
            .font: UIFont.systemFont(ofSize: 20)
        ]
        gameScoreLabel.attributedText = NSAttributedString(string: title, attributes: attributes)
    }
    
    // MARK: - Updating UI
    
    /// Updates UI: buttons, labels, 'cardStackView'.
    private func updateViewFromModel(for action: GameAction) {
        updateGameScoreLabel()
        updateButtons()
        updateCardViewsStatus()
        updateCardViewsWithCardsOnTable(for: action)
    }
    
    private enum GameAction {
        case touching, dealing, shuffling, highlighting
    }
    
    /// Updates UI of all the buttons, depending on the game state.
    private func updateButtons() {
        // New Game
        if game.cardsOnTable.isEmpty {
            newGameButton.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        } else {
            newGameButton.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1)
        }
        
        // Hint
        if  game.hintedCards.isEmpty &&
            game.cardsOnTable.count > Game.GameConstants.numberOfCardsInSet &&
            game.selectedCards.count != Game.GameConstants.numberOfCardsInSet {
            hintButton.isEnabled = true
        } else {
            hintButton.isEnabled = false
        }
        
        // Deal 3 cards
        if  game.selectedCards.count != Game.GameConstants.numberOfCardsInSet &&
            !game.cardsInDeck.isEmpty {
            dealThreeCardsButton.isEnabled = true
        } else {
            dealThreeCardsButton.isEnabled = false
        }
        
        // New Cards Pile
        if game.cardsInDeck.isEmpty {
            newCardsPileButton!.layer.borderColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1)
            newCardsPileButton!.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        } else {
            newCardsPileButton!.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            newCardsPileButton!.layer.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1)
        }
        
        // Discarded Cards Pile
        if game.matchedCards.isEmpty {
            discardCardsPileButton!.layer.borderColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1)
            discardCardsPileButton!.layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        } else {
            discardCardsPileButton!.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            discardCardsPileButton!.layer.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1)
        }
    }
    
    /// Goes through 'CardViews' on table and updates its 'status'.
    private func updateCardViewsStatus() {
        for cardView in cardStackView.cardViews {
            let card = cardView.card!
            
            if game.selectedCards.contains(card) {
                if game.selectedCards.count < Game.GameConstants.numberOfCardsInSet {
                    cardStackView.setCardViewState(.selected, for: card)
                } else {
                    cardStackView.setCardViewState(.mismatched, for: card)
                }
            } else if game.hintedCards.contains(card) {
                cardStackView.setCardViewState(.hinted, for: card)
            } else if game.matchedCards.contains(card) {
                cardStackView.setCardViewState(.matched, for: card)
            } else {
                cardStackView.setCardViewState(.normal, for: card)
            }
        }
    }
    
    private weak var timer: Timer?
    
    /// Adds 'CardViews' with new cards; Removes 'CardViews' with matched cards.
    private func updateCardViewsWithCardsOnTable(for action: GameAction) {
        
        switch action {
        case .touching:
            // check for a "Set", if match didn't happen, don't do anything
            guard game.cardsOnTable != cardStackView.cardViews.map({ $0.card! }) else { break }
            
            // force matched cards bouncing around
            cardStackView.matchedCards.forEach { cardView in
                cardView.needsToRemoveFromTable = true
                cardBehavior.bounceAround(cardView)
            }
            
            // remove matched cards performing flyaway animation
            timer?.invalidate()
            timer = Timer.scheduledTimer(
                withTimeInterval: 3.0,
                repeats: false,
                block: { _ in
                    self.cardStackView.matchedCards.forEach { cardView in
                        self.cardBehavior.snapToDiscardedPile(cardView)
                    }
                }
            )
            fallthrough
        case .dealing:
            // add new ones
            game.cardsOnTable.forEach {
                if cardStackView.getCardView(with: $0) == nil {
                    cardStackView.addCardView(with: $0)
                    addTapGesture(to: cardStackView.getCardView(with: $0)!)
                }
            }
            cardBehavior.updateCardsOnTable(in: cardStackView)
        case .shuffling:
            // rearrange cards after shuffling
            game.cardsOnTable.forEach { card in
                if let cardView = cardStackView.getCardView(with: card) {
                    cardStackView.sendSubview(toBack: cardView)
                }
            }
            cardBehavior.updateCardsOnTable(in: cardStackView)
        case .highlighting:
            // for highlighting don't do anything here
            break
        }
    }
    
    // MARK: - Gestures
    
    /// Adds a tap gesture to the given 'CardView'.
    private func addTapGesture(to cardView: CardView) {
        let tap = UITapGestureRecognizer(
            target: self,
            action: #selector(touchCardView(_:))
        )
        cardView.addGestureRecognizer(tap)
    }
    
    /// Simulates a touch of the card on table.
    @objc private func touchCardView(_ recognizer: UITapGestureRecognizer) {
        guard let cardView = recognizer.view as? CardView else { return }
        
        switch recognizer.state {
        case .changed, .ended:
            game.select(card: cardView.card!)
            updateViewFromModel(for: .touching)
        default:
            break
        }
    }
    
    /// Rearranges cards on table.
    @objc private func shuffleCards(_ recognizer: UIRotationGestureRecognizer) {
        guard game.selectedCards.count != Game.GameConstants.numberOfCardsInSet else { return }
        guard !game.cardsOnTable.isEmpty else { return }
        
        switch recognizer.state {
        case .ended:
            game.shuffleCards(.onTable)
            updateViewFromModel(for: .shuffling)
        default:
            break
        }
    }
    
    // MARK: - View Controller Lifecycle
    
    // This will be called when view's bounds change, but not only
    // Recalculate position of minidecks
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // Rectangles for animation when dealing cards
        cardStackView.newCardsStartFrom = cardStackView.convert(newCardsPileButton!.frame, from: newCardsPileButton!.superview)
        
        // Rectangles for animation when discarding cards
        cardStackView.discardedCardsGoTo = cardStackView.convert(discardCardsPileButton!.frame, from: discardCardsPileButton!.superview)
    }
    
    // rearrange cards on device rotation
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        // skip animation, if the game hasn't started yet
        guard !game.cardsOnTable.isEmpty else { return }
        
        coordinator.animate(
            alongsideTransition: nil,
            completion: { finished in
                self.cardBehavior.updateCardsOnTable(in: self.cardStackView)
            }
        )
    }
}

// MARK: - UIDynamicAnimatorDelegate

extension SetViewController: UIDynamicAnimatorDelegate {
    
    /// Flips card over when it reaches discarded pile.
    func dynamicAnimatorDidPause(_ animator: UIDynamicAnimator) {
        animator.items(in: view.bounds).forEach { item in
            if let cardView = item as? CardView, cardView.needsToRemoveFromTable {
                UIViewPropertyAnimator.runningPropertyAnimator(
                    withDuration: 0,
                    delay: 0,
                    options: [],
                    animations: {},
                    completion: cardBehavior.flipOver(cardView)
                )
            }
        }
    }
}
