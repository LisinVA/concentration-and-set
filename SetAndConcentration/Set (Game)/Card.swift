//
//  Card.swift
//  Set (Game)
//
//  Created by zweqi on 29/01/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import Foundation

// the Model of the Card

struct Card {
    
    // MARK: - Public Properties
    
    let numberOfSymbols: Int
    let symbol: Int
    let shading: Int
    let color: Int
    
    var description: String {
        return "Card(numberOfSymbols: \(numberOfSymbols), symbol: \(symbol), shading: \(shading), color: \(color))"
    }
    
    // MARK: - Card Features
    
    private typealias Features = (numberOfSymbols: Int, symbol: Int, shading: Int, color: Int)
    
    private static var featuresFactory = [Features]()
    
    // builds an array of `Features` filled with unique tuples of random values in the given range
    private static func makeFeaturesFactory() {
        let range = Card.featuresRange
        
        for numberOfSymbols in range {
            for symbol in range {
                for shading in range {
                    for color in range {
                        featuresFactory.append((numberOfSymbols, symbol, shading, color))
                    }
                }
            }
        }
    }
    
    private static func getFeatures() -> Features {
        if featuresFactory.isEmpty {
            makeFeaturesFactory()
        }
        return featuresFactory.removeLast()
    }
    
    // MARK: - Initialization
    
    init() {
        (numberOfSymbols, symbol, shading, color) = Card.getFeatures()
    }
}

// MARK: - Equatable

extension Card: Equatable {
    
    static func ==(lhs: Card, rhs: Card) -> Bool {
        return  lhs.numberOfSymbols == rhs.numberOfSymbols &&
                lhs.symbol == rhs.symbol &&
                lhs.shading == rhs.shading &&
                lhs.color == rhs.color
    }
}

// MARK: - Constants

extension Card {
    private static let featuresRange = 0...2
}
