//
//  Game.swift
//  Set (Game)
//
//  Created by zweqi on 29/01/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import Foundation

// the Model of the Game

struct Game {
    
    // MARK: - Private Properties
    
    private(set) var cardsInDeck = [Card]()
    private(set) var cardsOnTable = [Card]()
    private(set) var matchedCards = [Card]()
    private(set) var selectedCards = [Card]()
    private(set) var hintedCards = [Card]()
    
    private(set) var currentTurn = DateInterval(start: Date(), end: Date())
    private(set) var score = 0
    
    // MARK: - Common Actions
    
    /// Deals cards, moving them from the deck to the table;
    /// removes selection;
    /// changes `score` if "deal three more cards" option is used.
    ///
    /// Precondition: `numberOfCards` > 0
    mutating func deal(numberOfCards: Int) {
        precondition(numberOfCards > 0, "Game.deal(numberOfCards: \(numberOfCards)), number of cards must be > 0")
        
        // penalty for using "deal 3 cards" option when you can form a "Set"
        if numberOfCards == GameConstants.numberOfCardsInSet, let _ = formSet(from: cardsOnTable) {
            addPoints(for: .dealThreeCards)
            selectedCards.removeAll()
        }
        
        // deal cards
        for _ in 1...numberOfCards {
            if !cardsInDeck.isEmpty {
                cardsOnTable.append(cardsInDeck.removeFirst())
            }
        }
    }
    
    /// Removes selecte cards from the table and put it right to the matched cards.
    private mutating func moveFromSelectedToMatched(cards: [Card]) {
        matchedCards.removeAll()
        for card in cards {
            if let index = cardsOnTable.index(of: card) {
                matchedCards.append(cardsOnTable.remove(at: index))
            }
        }
        selectedCards.removeAll()
    }

    enum CardsPlacement {
        case inDeck, onTable
    }
    
    /// Shuffles all cards in `cardsInDeck`.
    mutating func shuffleCards(_ placement: CardsPlacement) {
        if placement == .onTable {
            guard selectedCards.count != GameConstants.numberOfCardsInSet else { return }
        }

        var cardsToShuffle = (placement == .inDeck ? cardsInDeck : cardsOnTable)
        guard !cardsToShuffle.isEmpty else { return }
        
        var shuffledCards = [Card]()
        for _ in 1...cardsToShuffle.count {
            let randomCard = cardsToShuffle.remove(at: cardsToShuffle.count.arc4random)
            shuffledCards.append(randomCard)
        }
        
        switch placement {
        case .inDeck: cardsInDeck = shuffledCards
        case .onTable: cardsOnTable = shuffledCards
        }
        selectedCards.removeAll()
    }
    
    /// Performs a "selection/deselection" of cards, changing `score` according to game rules.
    ///
    /// Precondition: `card` must be from the `cardsOnTable`
    mutating func select(card: Card) {
        precondition(cardsOnTable.contains(card), "Game.select(card: \(card)), chosen card is not on the table")
        
        // clear selection after previously not matched "Set"
        if selectedCards.count == GameConstants.numberOfCardsInSet {
            selectedCards.removeAll()
        }
        
        // select/deselect a card
        if selectedCards.contains(card) {
            selectedCards.remove(at: selectedCards.index(of: card)!)
            addPoints(for: .deselection)
        } else {
            selectedCards.append(card)
        }
        
        // check for "Set"
        if selectedCards.count == GameConstants.numberOfCardsInSet {
            if canFormSet(from: selectedCards) {
                // remove matched "Set" and deal a new one
                moveFromSelectedToMatched(cards: selectedCards)
                
                for _ in 1...GameConstants.numberOfCardsInSet {
                    deal(numberOfCards: 1)
                }
                addPoints(for: .selectionMatched)
            } else {
                // "Set" didnt match
                addPoints(for: .selectionDidNotMatch)
            }
        }
    }
    
    private enum CardAction {
        case deselection, dealThreeCards, selectionMatched, selectionDidNotMatch, hint
    }
    
    /// Changes `score` of the current player depending on the action:
    /// * `deselection` (-1)
    /// * `dealThreeCards` (-1)
    /// * `selectionMatched` (+3 / +1) \*
    /// * `selectionDidNotMatch` (-5)
    /// * `hint` (-1)
    ///
    /// \* (+1) If current turn is longer than a 3 seconds.
    private mutating func addPoints(for action: CardAction) {
        switch action {
        case .deselection:
            score += GamePoints.deselection
        case .dealThreeCards:
            score += GamePoints.dealThreeCards
        case .hint:
            score += GamePoints.hint
        case .selectionDidNotMatch:
            currentTurn.end = Date()
            score += GamePoints.didNotMatch
        case .selectionMatched:
            currentTurn.end = Date()
            score += currentTurn.duration < GameConstants.timeLimitForMatchMax ?
                GamePoints.matchMax : GamePoints.matchMin
        }
    }
    
    /// Returns three random matching `cards`, taken from the given cards, that form a "Set";
    /// if there is no "Set" available in the given cards, returns nil.
    ///
    /// Precondition: `cards.count` >= `GameConstants.minNumberOfCardsInSet`
    func formSet(from cards: [Card]) -> [Card]? {
        precondition(cards.count >= GameConstants.numberOfCardsInSet, "Game.formSet(from cards: \(cards)). There must be at least three cards to from a set.")
        
        for card1 in cards {
            for card2 in cards {
                for card3 in cards {
                    if card1 != card2 && card1 != card3 && card2 != card3 {
                        if canFormSet(from: [card1, card2, card3]) {
                            return [card1, card2, card3]
                        }
                    }
                }
            }
        }
        return nil
    }
    
    /// Returns a Boolean value indicating whether the given `cards` form a "Set".
    ///
    /// Precondition: `cards.count` == `GameConstants.minNumberOfCardsInSet`
    func canFormSet(from cards: [Card]) -> Bool {
        precondition(cards.count == GameConstants.numberOfCardsInSet, "Game.canFormSet(from cards: \(cards). There must be exactly three cards to check.")
        
        let card1 = cards[0], card2 = cards[1], card3 = cards[2]
        
        let numberOfSymbols = (card1.numberOfSymbols == card2.numberOfSymbols) && (card1.numberOfSymbols == card3.numberOfSymbols)
        let symbols = (card1.symbol == card2.symbol) && (card1.symbol == card3.symbol)
        let shading = (card1.shading == card2.shading) && (card1.shading == card3.shading)
        let color = (card1.color == card2.color) && (card1.color == card3.color)
        
        return numberOfSymbols || symbols || shading || color
    }

    /// Adds three random matching cards on the table to `hintedCards`, if there is any.
    mutating func addHint() {
        if cardsOnTable.count >= GameConstants.numberOfCardsInSet, let cards = formSet(from: cardsOnTable) {
            selectedCards.removeAll()
            hintedCards.append(contentsOf: cards)
            addPoints(for: .hint)
        }
    }
    
    mutating func removeHint() {
        hintedCards.removeAll()
    }
    
    // MARK: - Initizalization
    
    /// Creates a shuffled deck of cards.
    ///
    /// Precondition: `numberOfCards` >= `GameConstants.numberOfCardsFirstDeal`
    init(numberOfCards: Int) {
        precondition(numberOfCards >= GameConstants.numberOfCardsFirstDeal, "Game.init(numberOfCards: \(numberOfCards)), number of cards must be >= \(GameConstants.numberOfCardsFirstDeal)")
        
        for _ in 1...numberOfCards {
            cardsInDeck.append(Card())
        }
        shuffleCards(.inDeck)
    }
}

// MARK: - Constants

extension Game {
    struct GameConstants {
        static let maxNumberOfCardsInDeck = 81
        static let numberOfCardsFirstDeal = 12
        static let numberOfCardsInSet = 3
        static let standardTurnLength: TimeInterval = 5.0
        static let bonusTime: TimeInterval = 3.0
        static let timeLimitForMatchMax: TimeInterval = 3.0
    }
    struct GamePoints {
        static let deselection = -1
        static let dealThreeCards = -1
        static let hint = -1
        static let didNotMatch = -5
        static let matchMax = 3
        static let matchMin = 1
    }
}
