//
//  CardStackView.swift
//  Set (Game)
//
//  Created by zweqi on 29/01/2018.
//  Copyright © 2018 zweqi. All rights reserved.
//

import UIKit

// utility object to create and manage card views

class CardStackView: UIView, UIDynamicAnimatorDelegate {
    
    var newCardsStartFrom: CGRect!
    var discardedCardsGoTo: CGRect!
    
    var grid: Grid {
        return Grid(frame: bounds,
                    cellCount: oldCardsToRearrange.count + newCardsToDeal.count,
                    aspectRatio: CardView.SizeRatio.cardWidthToHeightRatio)
    }
    
    var cardViews: [CardView] {
        return subviews.filter { $0 is CardView } as! [CardView]
    }
    
    var newCardsToDeal: [CardView] {
        return cardViews.filter { $0.brandNew && !$0.needsToRemoveFromTable }
    }
    
    var oldCardsToRearrange: [CardView] {
        return cardViews.filter { !$0.brandNew && !$0.needsToRemoveFromTable }
    }
    
    var cardsToRemoveFromTable: [CardView] {
        return cardViews.filter { $0.needsToRemoveFromTable }
    }
    
    var matchedCards: [CardView] {
        return cardViews.filter { $0.state == .matched }
    }
    
    func addCardView(with card: Card) {
        guard subviews.count < Game.GameConstants.maxNumberOfCardsInDeck else { return }
        guard getCardView(with: card) == nil else { return }
        
        let cardView = CardView(card: card, cardStackView: self, rect: newCardsStartFrom)
        addSubview(cardView)
    }
    
    func getCardView(with card: Card) -> CardView? {
        return cardViews.filter { $0.card == card }.first
    }
    
    func setCardViewState(_ state: CardView.CardState, for card: Card) {
        if let cardView = getCardView(with: card) {
            cardView.state = state
        }
    }
}

